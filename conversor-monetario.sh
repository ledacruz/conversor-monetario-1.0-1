#!/bin/bash

URL="http://api.exchangeratesapi.io/latest?base=$1&symbols=$2"

COTACAO=`curl $URL | jq ".rates.$2"`

VALORFIM=`python -c 'print ('$COTACAO' * '$3')'`

echo $VALORFIM
